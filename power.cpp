#include "power.h"
#include <iostream>

namespace otus_algo
{

double powerIterate(double basis, unsigned long long exponent)
{
    double result = 1;
    for(unsigned long long i = 0; i < exponent; ++i)
    {
        result *= basis;
    }
    return result;
}

double powerByExponentSeparate(double basis, unsigned long long exponent)
{
    if(exponent < 1)
    {
        return 1;
    }

    double result = 1;
    if(exponent > 2)
    {
        while(exponent % 2 == 0)
        {
            exponent /= 2;
            result = (result == 1) ? (basis * basis) : (result * result);
        }
    }
    return powerIterate(result, exponent);
}

double powerByDigitsDecay(double basis, unsigned long long exponent)
{
    if(exponent < 1)
    {
        return 1;
    }

    double d = basis;
    double result = 1;
    while(exponent > 1)
    {
        if(exponent & 1)
        {
            result *= d;
        }
        d *= d;
        exponent >>= 1;
    }
    return result * d;
}

}
