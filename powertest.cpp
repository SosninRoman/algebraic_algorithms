#include "powertest.h"
#include <functional>
#include <chrono>
#include "power.h"
#include "testdatareader.h"
#include <iostream>

namespace otus_algo
{

std::ostream& addSpace(std::ostream& stream)
{
    stream << std::string(5, ' ');
    return stream;
}

bool areDoubleSame(double dFirstVal, double dSecondVal)
{
    return std::fabs(dFirstVal - dSecondVal) < 1E-3;
}

bool powerTest(int testCaseNum, const std::function<double(double, unsigned long long)>& algorithm)
{
    const auto testData = otus_algo::readPowerTestData(testCaseNum);

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    const auto result = algorithm(testData.base, testData.exponent);
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

    const bool accepted = areDoubleSame(testData.expected, result);

    std::cout << otus_algo::testName(testCaseNum) << " : " << (accepted ? "ACCEPTED" : "FAILED") << std::endl
              << addSpace << "base: " << testData.base << " exponent: " << testData.exponent << std::endl
              << addSpace << "expected: " << testData.expected << std::endl
              << addSpace << "result: " << result << std::endl
              << addSpace << "time: " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << " ms" << std::endl
              << std::endl;

    return accepted;
}

void iterateTest(int maxTestCase)
{
    std::cout << "powerIterate power algorithm test" << std::endl;
    std::cout << "Test cases: [0;" << maxTestCase <<"]" << std::endl;
    for(int i = 0; i <= maxTestCase; ++i)
    {

        if(!powerTest(i, otus_algo::powerIterate))
        {
            return;
        }
    }
}

void decayWithMultiplyTest(int maxTestCase)
{
    std::cout << "powerByExponentSeparate power algorithm test" << std::endl;
    std::cout << "Test cases: [0;" << maxTestCase <<"]" << std::endl;
    for(int i = 0; i <= maxTestCase; ++i)
    {

        if(!powerTest(i, otus_algo::powerByExponentSeparate))
        {
            return;
        }
    }
}

void digitsDecayTest(int maxTestCase)
{
    std::cout << "powerByDigitsDecay power algorithm test" << std::endl;
    std::cout << "Test cases: [0;" << maxTestCase <<"]" << std::endl;
    for(int i = 0; i <= maxTestCase; ++i)
    {
        if(!powerTest(i, otus_algo::powerByDigitsDecay))
        {
            return;
        }
    }
}

void powerTests()
{
    iterateTest(9);
    decayWithMultiplyTest(9);
    digitsDecayTest(9);
}

}
