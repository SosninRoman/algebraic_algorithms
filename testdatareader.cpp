#include "testdatareader.h"
#include <filesystem>
#include <fstream>

namespace otus_algo
{

std::string testName(int N)
{
    return std::string("test.").append(std::to_string(N));
}

PowerTestCase readPowerTestData(int testNum)
{
    const auto testNameStr = testName(testNum);

    PowerTestCase result;

    std::fstream in(std::filesystem::path(POWER_TEST_DATA_PATH) / (testNameStr + ".in"));
    in >> result.base >>result.exponent;
    in.close();

    std::fstream out(std::filesystem::path(POWER_TEST_DATA_PATH) / (testNameStr + ".out"));
    out >> result.expected;
    out.close();

    return result;
}

}
