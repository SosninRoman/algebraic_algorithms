#ifndef TESTDATAREADER_H
#define TESTDATAREADER_H

#include <string>

namespace otus_algo
{

//Сформировать имя теста по его номеру
std::string testName(int testNum);

struct PowerTestCase
{
    double base = 0;
    unsigned long long exponent = 0;
    double expected = 0;
};

//Прочитать данные теста алгоритма возведения в степень
//first - значение теста
//second - значение результата
PowerTestCase readPowerTestData(int testNum);

}

#endif
