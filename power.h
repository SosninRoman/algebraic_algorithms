#ifndef POWER_H
#define POWER_H

namespace otus_algo
{

//Итеративное возведение в степень
double powerIterate(double basis, unsigned long long exponent);

//Степень двойки с домножением
double powerByExponentSeparate(double basis, unsigned long long exponent);

//Двоичное разложение показателя степени
double powerByDigitsDecay(double basis, unsigned long long exponent);

}

#endif
